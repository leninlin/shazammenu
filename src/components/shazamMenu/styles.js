import { Dimensions } from 'react-native';

const styles = {
  mainView: {
    backgroundColor: '#000',
    flex: 1,
  },
  runner: {
    position: 'absolute',
    backgroundColor: '#f00',
    borderRadius: 20,
  },
  menuView: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: Dimensions.get('window').width,
    height: 50,
  },
  menuList: {
    flexDirection: 'row',
  },
  menuItem: {
    padding: 10,
  },
  scrollView: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  }
}

export default styles;
