// React
import React, { Component } from 'react';

// UI
import {
  Dimensions,
  View,
  ScrollView,
  Text,
  Animated,
} from 'react-native';

import styles from './styles';

class ShazamMenu extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedSreen: 0,
      imgRef: null,
      screenOpacity: [1],
      left: new Animated.Value(0),
      top: new Animated.Value(0),
      width: new Animated.Value(0),
      height: new Animated.Value(0),
      bgScale: new Animated.Value(1.1),
      bgBlur: new Animated.Value(0),
    };

    this.anim = {
      left: { next: 0, prev: 0 },
      top: { next: 0, prev: 0 },
      width: { next: 0, prev: 0 },
      height: { next: 0, prev: 0 },
      bgScale: { next: 0, prev: 0, screen: 0 },
      bgBlur: { next: 0, prev: 0, screen: 0 },
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.refs['item0'].measure((fx, fy, width, height, px, py) => {
        this.setState({
          left: new Animated.Value(px),
          top: new Animated.Value(px),
          width: new Animated.Value(width),
          height: new Animated.Value(height),
        });
        
        setInterval(() => {
          Object.keys(this.anim).map(paramName => {
            let param = this.anim[paramName];
            if (param.screen !== undefined && this.state.selectedSreen != param.screen)
              return;
            if (param.prev != param.next) {
              Animated.spring(
                this.state[paramName],
                {
                  toValue: param.next,
                }
              ).start();
              param.prev = param.next;
            }
          });
        }, 10);
      });
    }, 100);
  }
      
  setBeginer(page, d) {
    this.refs['item' + page].measure((fx, fy, width1, height1, px1, py1) => {
      if (d > 0 && this.refs['item' + (page + 1)]) {
        this.refs['item' + (page + 1)].measure((fx, fy, width2, height2, px2, py2) => {
          this.anim.left.next = px1 + (px2 - px1)*d;
          this.anim.top.next = py1 + (py2 - py1)*d;
          this.anim.width.next = width1 + (width2 - width1)*d;
          this.anim.height.next = height1 + (height2 - height1)*d;
          this.anim.bgScale.next = 1.1 - ((page ? 1 : d)*0.1);
          this.anim.bgBlur.next = (page ? 1 : d);

          let { screenOpacity } = this.state;
          screenOpacity[page] = 1 - d;
          screenOpacity[page + 1] = d;
          this.setState({screenOpacity});

        });
      } else {
        this.anim.left.next = px1;
        this.anim.top.next = py1;
        this.anim.width.next = width1;
        this.anim.height.next = height1;
        this.anim.bgScale.next = 1.1 - ((page ? 1 : d)*0.1);
        this.anim.bgBlur.next = (page ? 1 : d);
      }
    });
  }

  getWidth() {
    return Dimensions.get('window').width;
  }

  render() {
    return (
      <View style={styles.mainView}>
        <Animated.Image
          source={this.props.backgroundImage}
          transition='transform'
          style={{
            transform: [
              {scaleX: this.state.bgScale},
              {scaleY: this.state.bgScale}
            ]
          }}
        />
        <View style={styles.menuView}>
          <Animated.View
            style={{
              left: this.state.left,
              top: this.state.top,
              width: this.state.width,
              height: this.state.height,
              ...styles.runner
            }}
          ></Animated.View>
          <View style={styles.menuList}>
            {this.props.screens.map((Screen, index) => (
              <Text
                ref={'item' + index}
                key={index}
                style={styles.menuItem}
              >
                {Screen.menuTitle}
              </Text>
            ))}
          </View>
        </View>
        <View style={styles.scrollView}>
          <ScrollView
            automaticallyAdjustContentInsets={false}
            scrollEventThrottle={1}
            horizontal={true}
            pagingEnabled={true}
            showsHorizontalScrollIndicator={false}
            bounces={false}
            onScroll={(e) => {
              let page = Math.floor(e.nativeEvent.contentOffset.x / e.nativeEvent.layoutMeasurement.width);
              if (page != this.state.selectedSreen) this.setState({ selectedSreen: page });
              this.setBeginer(page, (e.nativeEvent.contentOffset.x / e.nativeEvent.layoutMeasurement.width) - page);
            }}
            scrollsToTop={false}
          >
            {this.props.screens.map((Screen, index) => (
              <Animated.View 
                key={index}
                transition='opasity'
                style={{
                  opacity: this.state.screenOpacity[index]
                }}
              >
                <Screen  />
              </Animated.View>
            ))}
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default ShazamMenu;
