import thunk from 'redux-thunk';
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';

import sample from './reducers/sample';

const appReducers = combineReducers({
  sample
});

const configureStore = (initialState) => {
  const develop = compose(
    applyMiddleware(thunk),
    global.reduxNativeDevTools ?
      global.reduxNativeDevTools() :
      noop => noop,
  );

  const production = compose(applyMiddleware(thunk));

  const store = createStore(appReducers, initialState, global.__DEV__ ? develop : production);
  if (global.reduxNativeDevTools) {
    global.reduxNativeDevTools.updateStore(store);
  }
  return store;
};

const store = configureStore();

export default store;
