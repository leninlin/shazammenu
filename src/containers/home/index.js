// React
import React, { Component } from 'react';

// UI
import {
  View,
} from 'react-native';

// Redux
import { connect } from 'react-redux';

import styles from './styles';

import ShazamMenu from '../../components/shazamMenu';

import FirstScreen from '../firstScreen';
import SecondScreen from '../secondScreen';
import ThirdScreen from '../thirdScreen';

import bgImg from '../../../assets/bgImg.jpg';

class HomeScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
    }
  }

  render() {
    return (
      <View style={styles.mainView}>
        <ShazamMenu
          backgroundImage={bgImg}
          screens={[
            FirstScreen,
            SecondScreen,
            ThirdScreen
          ]}
        />
      </View>
    );
  }
}

HomeScreen.navigationOptions = {
  title: 'Домашняя страница',
};

export default connect()(HomeScreen);
