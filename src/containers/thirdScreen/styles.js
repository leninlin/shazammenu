const styles = {
  mainView: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  exampleBlock: {
    backgroundColor: '#0000ff',
    width: 300,
    height: 100,
    margin: 50,
  }
}
  
export default styles;
