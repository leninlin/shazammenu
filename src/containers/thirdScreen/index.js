// React
import React, { Component } from 'react';

// UI
import {
  Dimensions,
  View,
} from 'react-native';

// Redux
import { connect } from 'react-redux';

import styles from './styles';

class ThirdScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
    }
  }

  getWidth() {
    return Dimensions.get('window').width;
  }

  render() {
    return (
      <View style={styles.mainView}>
        <View style={styles.exampleBlock}></View>
      </View>
    );
  }
}

ThirdScreen.menuTitle = 'Последний';

export default connect()(ThirdScreen);
