// Navigation
import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from '../containers/home';

export const AppNavigator = createStackNavigator(
  {
    Home: { screen: HomeScreen },
  },
  {
    headerMode: 'none'
  }
);

export default createAppContainer(AppNavigator);
